<h1 align="center">create-mini-quick</h1>
<p align="center">约定代替编码，更灵活、迅速的脚手架搭建工具（mini版）</p>

<p align="center">
  <a href="">文档网站（暂无）</a>
  &nbsp;
</p>



---



## 介绍

create-mini-quick是 <a href="https://gitee.com/xuanxiaoqian/create-quick">create-quick</a>最小化demo





<br />

## 快速上手

第一步：clone本仓库：

```sh
git clone https://gitee.com/lianxuan7/create-quick.git
```



第二步：安装依赖和测试：

```sh
# 安装依赖
npm i # pnpm i

# 打包文件
npm run build

# 运行脚手架
node outfile.cjs
```

- templates文件夹里面的第一层为模板名，例如：templates/vue3、templates/nest
- 模板名里面有着三个文件夹，分别是`base`、`options`、`ejs`（可通过配置变量自定义命名）
- options文件夹里面的各种选项里面的文件目录需要跟base目录一一对应
- options里面的`package.json`会和base的`package.json`进行合并，其他文件全是替换
- options里面的`ejsData.js`为参数变量，用于给ejs模板引擎渲染的，该文件也不会添加到base里（可通过配置变量自定义命名）
- ejs文件夹为模板引擎，里面的文件目录也是和base目录一一对应，例如：`main.ts.ejs`文件名最终合并到base里面的名字为`main.ts`，也会覆盖base



第三步：使用者只需要关心根目录下的`templatesData.json`询问数据和`template`模板文件夹即可：

查看`templates`文件夹下面有两个文件夹`uniapp-vue3`和`vue3`，它们分别是不同的项目模板，我们找到`vue3`模板来介绍：

`vue3`文件夹下面有三个文件夹`base`、`ejs`、`options`，它们分别是基础项目、引入配置、项目选项

`base`我们就不需要介绍了，它是一个项目的最基本架构，必不可少

`ejs`是什么呢？举个例子：我们vue3项目有可选配置`pinia`/`vue-router`，它们如果需要使用是不是需要在`main.ts`文件中引入？对，`ejs`文件夹就是这样的一个作用，我们使用了ejs模板引擎进行渲染：

```ts
// main.ts.ejs
<%- importList -%>
import { createApp } from 'vue'
import App from './App.vue'

import '@/style/reset.css'

const app = createApp(App)

<%- useList -%>

app.mount('#app')
```

`options`文件夹就是可选的配置，里面的目录结构需要和`base`文件夹一一对应，里面除了`package.json`是和`base`合并，其他都是覆盖。值得一提的是`options`文件夹下面还可以有一个`config-text.js`文件，这个是什么呢？这个就是`ejs`的数据，也不会添加到`base`目录

```js
// config-text.js

module.exports = {
    descripts: 'pinia',
    importList: `import pinia from '@/store/index' \n`,
    useList: `app.use(pinia) \n`,
    callback(traget, config) {	// 当前options复制之后会调用这个函数
        console.log(traget)
        console.log(config)
    }
}
```


`templateData.json`是询问需要的数据：

```json
// templatesData.json
{
  "vue3": {
    "title": "vue3 + vue-router",
    "options": [
      {
        "title": "pinia状态管理",
        "value": "pinia"
      },
      {
        "title": "prettier格式化",
        "value": "prettier"
      },
      {
        "title": "vue-router路由",
        "value": "vue-router"
      }
    ]
  },
  "uniapp-vue3": {
    "title": "uniapp的vue3版本",
    "options": []
  }
}
```



<br />

## 快速上手

前往最小化demo仓库   <a href="https://gitee.com/lianxuan7/create-quick">create-mini-quick</a>







<br />

## 疑问交流

QQ群: <a target="_blank" href="https://qm.qq.com/cgi-bin/qm/qr?k=LrFpPFoHAHFikBUJQqKjViRJIY1BH250&jump_from=webapi">qian-cli(746382337)</a>
